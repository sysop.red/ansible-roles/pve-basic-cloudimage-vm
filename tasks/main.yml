---
# tasks file for vm-base
# https://docs.ansible.com/ansible/latest/collections/community/general/proxmox_kvm_module.html


- name: "{{ config.title }} - Create VM (linked clone)"
  community.general.proxmox_kvm:
    api_user: "{{ proxmox.api_user }}"
    api_password: "{{ proxmox_api_password }}"
    api_host: "{{ inventory_hostname }}"
    node: "{{ proxmox.hostname }}"
    clone: arbitrary_name
    vmid: "{{ template_id }}"
    newid: "{{ config.vmid }}"
    name: "{{ config.name }}"
    # storage: local
    full: false
    format: unspecified
    timeout: 500
  register: vm_status

# - name: interfaces
#   ansible.builtin.debug:
#     msg: "{{ vm_status }}"

- name: "{{ config.title }} - Pause for {{timers.wait_for_vm_creation_sec}}sec to allow VM creation"
# "todo: check https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html#do-until-loops
  ansible.builtin.wait_for:
    timeout: "{{ timers.wait_for_vm_creation_sec }}"
  delegate_to: localhost
  when: vm_status.changed

- name: "{{ config.title }} - Resize disk"
  ansible.builtin.command:
    cmd: "qm resize {{ config.vmid }} scsi0 {{ config.disk }}G"
  when: vm_status.changed

- name: "{{ config.title }} - Delete NIC net0"
  community.general.proxmox_nic:
    api_user: "{{ proxmox.api_user }}"
    api_password: "{{ proxmox_api_password }}"
    api_host: "{{ inventory_hostname }}"
    name: "{{ config.name }}"
    interface: net0
    state: absent
  when: vm_status.changed

- name: "{{ config.title }} - Add NIC net0"
  community.general.proxmox_nic:
    api_user: "{{ proxmox.api_user }}"
    api_password: "{{ proxmox_api_password }}"
    api_host: "{{ inventory_hostname }}"
    name: "{{ config.name }}"
    interface: net0
    bridge: "{{ proxmox.lan_bridge }}"
    mac: "{{ config.lan.mac }}"
  when: vm_status.changed

- name: "{{ config.title }} - Add NIC net1"
  community.general.proxmox_nic:
    api_user: "{{ proxmox.api_user }}"
    api_password: "{{ proxmox_api_password }}"
    api_host: "{{ inventory_hostname }}"
    name: "{{ config.name }}"
    interface: net1
    bridge: "{{ proxmox.wan_bridge }}"
    mac: "{{ config.wan.mac }}"
  when: (vm_status.changed) and (config.wan.mac is defined)

- name: "{{ config.title }} - Set VM base config"
  community.general.proxmox_kvm:
    api_user: "{{ proxmox.api_user }}"
    api_password: "{{ proxmox_api_password }}"
    api_host: "{{ inventory_hostname }}"
    node: "{{ proxmox.hostname }}"
    name: "{{ config.name }}"
    update: true
    onboot: "{{ config.boot.autostart }}"
    startup: order={{ config.boot.order }}
    cores: "{{ config.cores }}"
    memory: "{{ config.memory }}"
    ciuser: "{{ user.username }}"
    cipassword: "{{ user.password }}"
    sshkeys: "{{ user.sshkey }}"
    searchdomains: "{{ hostvars[inventory_hostname].dns.search_domain | default('', true) }}"
    nameservers: "{{ hostvars[inventory_hostname].dns.nameserver | default('9.9.9.9', true) }}"
  when: vm_status.changed

- name: "{{ config.title }} - Set CPU type to Host"
  community.general.proxmox_kvm:
    api_user: "{{ proxmox.api_user }}"
    api_password: "{{ proxmox_api_password }}"
    api_host: "{{ inventory_hostname }}"
    node: "{{ proxmox.hostname }}"
    name: "{{ config.name }}"
    update: true
    cpu: host
  when: (vm_status.changed) and (force_cpu_type_host is defined) and (force_cpu_type_host)

- name: "{{ config.title }} - Set cloud-init network config (single network)"
  community.general.proxmox_kvm:
    api_user: "{{ proxmox.api_user }}"
    api_password: "{{ proxmox_api_password }}"
    api_host: "{{ inventory_hostname }}"
    node: "{{ proxmox.hostname }}"
    name: "{{ config.name }}"
    update: true
    ipconfig:
      ipconfig0: 'ip={{ config.lan.ip }},gw={{ hostvars[inventory_hostname].routing.lan.default_gw }}'
  when: (vm_status.changed) and (config.wan.mac is not defined)
  notify:
    - Update local SSH config

- name: "{{ config.title }} - Set cloud-init network config (dual network)"
  community.general.proxmox_kvm:
    api_user: "{{ proxmox.api_user }}"
    api_password: "{{ proxmox_api_password }}"
    api_host: "{{ inventory_hostname }}"
    node: "{{ proxmox.hostname }}"
    name: "{{ config.name }}"
    update: true
    ipconfig:
      ipconfig0: 'ip={{ config.lan.ip }}'
      ipconfig1: 'ip={{ config.wan.ip }}'
  when: (vm_status.changed) and (config.wan.mac is defined)
  notify:
    - Update local SSH config

- name: "{{ config.title }} - Start"
  community.general.proxmox_kvm:
    api_user: "{{ proxmox.api_user }}"
    api_password: "{{ proxmox_api_password }}"
    api_host: "{{ inventory_hostname }}"
    node: "{{ proxmox.hostname }}"
    name: "{{ config.name }}"
    newid: "{{ config.vmid }}"
    state: started

# @todo: add ssh key fingerprint to localhost

